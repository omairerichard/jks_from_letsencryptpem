# JKS_from_letsencryptPEM

Creates a java keystore from the PEM certificates provided by LetsEncrypt.
Execute as root, but all the work is done in a user's home directory. Edit the script to configure your user and paths.
The script is interactive for requesting passwords and the data for a temporary certicate used for creating the keystore. This temporary certificate is removed once the keystore is created, before adding the PEM certificate.

Dependencies: openssl and Java's keytool

