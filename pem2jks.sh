#!/bin/bash
USER=omairerichard
PATH_KEY=/etc/letsencrypt/live/prot-on.com/privkey.pem
PATH_CER=/etc/letsencrypt/live/prot-on.com/cert.pem
PATH_CHAIN=/etc/letsencrypt/live/prot-on.com/chain.pem
PATH_P12=/home/${USER}/letsencrypt/cert_web.p12
PATH_JKS=/home/${USER}/letsencrypt/web-keystore.jks

if test -f "$PATH_P12"; then
    if test -f "${PATH_P12}.bak"; then
        rm -f ${PATH_P12}.bak
    fi
    mv $PATH_P12 ${PATH_P12}.bak
fi
if test -f "$PATH_JKS"; then
    if test -f "${PATH_JKS}.bak"; then
        rm -f ${PATH_JKS}.bak
    fi
    mv $PATH_JKS ${PATH_JKS}.bak
fi

# PEM private and public to p12
cat $PATH_KEY $PATH_CER $PATH_CHAIN | openssl pkcs12 -export -out $PATH_P12

keytool -genkey -keyalg RSA -alias temp -keystore $PATH_JKS
keytool -delete -alias temp -keystore $PATH_JKS

# Import into keystore
keytool -v -importkeystore -srckeystore $PATH_P12 -srcstoretype pkcs12 -destkeystore $PATH_JKS 

# Change owner for copying from SCP
chown ${USER}:${USER} $PATH_JKS
